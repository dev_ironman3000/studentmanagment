<?php

use Illuminate\Database\Seeder;

class BatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batches = array (
			  0 => 
			  array (
			    'id' => '1',
			    'batch_name' => 'ITY-1(20)',
			    'class' => 'class 11th',
			    'language' => '',
			    'stream' => '',
			    'remarks' => '',
			    'batch_end_date' => '0000-00-00',
			    'batch_start_date' => '0000-00-00',
			    'created_at' => '2020-08-25 11:46:03',
			    'updated_at' => '2020-08-28 16:05:24',
			  ),
			  1 => 
			  array (
			    'id' => '2',
			    'batch_name' => 'IOY-1(20)',
			    'class' => 'class 12th',
			    'language' => '',
			    'stream' => '',
			    'remarks' => '',
			    'batch_end_date' => '0000-00-00',
			    'batch_start_date' => '0000-00-00',
			    'created_at' => '2020-08-25 11:46:53',
			    'updated_at' => '2020-08-28 16:05:45',
			  ),
			  2 => 
			  array (
			    'id' => '4',
			    'batch_name' => 'MTY-1(20)',
			    'class' => 'class 11th',
			    'language' => '',
			    'stream' => '',
			    'remarks' => '',
			    'batch_end_date' => '0000-00-00',
			    'batch_start_date' => '0000-00-00',
			    'created_at' => '2020-08-28 16:02:16',
			    'updated_at' => '2020-08-28 16:06:03',
			  ),
			  3 => 
			  array (
			    'id' => '5',
			    'batch_name' => 'ITY-2(20)',
			    'class' => 'class 11th',
			    'language' => '',
			    'stream' => '',
			    'remarks' => '',
			    'batch_end_date' => '0000-00-00',
			    'batch_start_date' => '0000-00-00',
			    'created_at' => '2020-08-28 16:03:03',
			    'updated_at' => '2020-08-28 16:06:18',
			  ),
			  4 => 
			  array (
			    'id' => '6',
			    'batch_name' => 'MTY-2(20)',
			    'class' => 'class 11th',
			    'language' => '',
			    'stream' => '',
			    'remarks' => '',
			    'batch_end_date' => '0000-00-00',
			    'batch_start_date' => '0000-00-00',
			    'created_at' => '2020-08-28 16:03:59',
			    'updated_at' => '2020-08-28 16:06:32',
			  ),
			  5 => 
			  array (
			    'id' => '7',
			    'batch_name' => 'MOY-1(20)',
			    'class' => 'class 12th',
			    'language' => '',
			    'stream' => '',
			    'remarks' => '',
			    'batch_end_date' => '0000-00-00',
			    'batch_start_date' => '0000-00-00',
			    'created_at' => '2020-08-28 16:05:05',
			    'updated_at' => '2020-08-28 16:05:05',
			  ),
			);

        foreach ($batches as $key => $batch) {
        	DB::table('batch')->updateOrInsert(['id'=>$batch['id']],$batch);
        }
    }
}
