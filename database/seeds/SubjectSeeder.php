<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$subjects = array (
			  0 => 
			  array (
			    'id' => '1',
			    'name' => 'Chemistry',
			    'subject_category' => 'iit',
			    'created_at' => '2020-08-25 11:44:16',
			    'updated_at' => '2020-08-28 16:02:03',
			  ),
			  1 => 
			  array (
			    'id' => '2',
			    'name' => 'Physics',
			    'subject_category' => 'iit',
			    'created_at' => '2020-08-25 11:44:47',
			    'updated_at' => '2020-08-28 16:01:50',
			  ),
			  2 => 
			  array (
			    'id' => '3',
			    'name' => 'Mathematics',
			    'subject_category' => 'iit',
			    'created_at' => '2020-08-25 11:45:19',
			    'updated_at' => '2020-08-28 16:03:19',
			  ),
			  3 => 
			  array (
			    'id' => '4',
			    'name' => 'Biology',
			    'subject_category' => 'Neet',
			    'created_at' => '2020-08-28 16:01:22',
			    'updated_at' => '2020-08-28 16:01:22',
			  ),
			  4 => 
			  array (
			    'id' => '5',
			    'name' => 'Science',
			    'subject_category' => 'Foundation',
			    'created_at' => '2020-09-10 18:45:10',
			    'updated_at' => '2020-09-10 18:45:10',
			  ),
			);

        foreach ($subjects as $key => $subject) {
        	DB::table('subjects')->updateOrInsert(['id'=>$subject['id']],$subject);
        }
    }
}
