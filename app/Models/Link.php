<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subject;
use App\Models\Link;
use Auth;
use DB;

class Link extends Model
{
    protected $table = "links";

    protected $fillable = [
        'link',
        'type',
        'start_at',
        'end_at',
        'title',
        'description',
        'subject_id',
        'chapter_id',
        'editedBy',
    ];   

    public function saveLink($Link){
        $reservationtime    = explode('-', $Link['reservationtime']);        
        $Link['start_at']   = date("Y-m-d H:i:s", strtotime(trim($reservationtime[0])));
        $Link['end_at']     = date("Y-m-d H:i:s", strtotime(trim($reservationtime[1])));
        $Link['editedBy']   = Auth::user()->id;
        $link = $this->updateOrCreate(['id' => $Link['id']] ,$Link);
        $link->batch()->sync($Link['batch_id']);
    }

    public function getAll($search=null){  
            // dd($search);
        return $this::join('batch_link','batch_link.link_id','links.id')
                    ->when(!empty($search['subjects']), function ($query) use($search) {
                            return $query->WhereIn('subject_id',$search['subjects']);
                        })
                    ->when(!empty($search['chapters']), function ($query) use($search){
                            return $query->WhereIn('chapter_id',$search['chapters']);
                        })
                    ->when(!empty($search['types']), function ($query) use($search){
                            return $query->WhereIn('type',$search['types']);
                        })
                    ->when(!empty($search['batchs']), function ($query) use($search){
                            return $query->WhereIn('batch_link.batch_id',$search['batchs']);
                        })
                    ->get();
    }

    public function getlinkBySubject($subject_id,$type){
        
        $currentDateTime = date('Y-m-d h:i:s');
       
       $Link = DB::table('batch_link')
                    ->select(
                        'batch_link.batch_id',
                        'batch_link.link_id',
                        'links.start_at',
                        'links.end_at',
                        'links.link',
                        'links.type',
                        'links.subject_id',
                        'links.title'
                    )
                    ->join('links','links.id','=','batch_link.link_id')
                    ->where('batch_link.batch_id', auth()->user()->student->batch_id)
                    ->where('subject_id', '=', $subject_id)
                    ->where('start_at','<=', $currentDateTime)
                    ->where('end_at','>=', $currentDateTime)
                    ->where('links.type', $type)
                    ->orderBy('start_at', 'desc')
                    ->get(); 
        //  dd($Link);       
        return $Link;
    } 

    public function getlinkByChapter($chapter_id,$type){
        $currentDateTime = date('Y-m-d h:i:s');
        $Link = DB::table('batch_link')
                ->select(
                        'batch_link.batch_id',
                        'batch_link.link_id',
                        'links.start_at',
                        'links.end_at',
                        'links.link',
                        'links.type',
                        'links.subject_id',
                        'links.title'
                )
                ->join('links','links.id','=','batch_link.link_id')
                ->where('batch_link.batch_id', auth()->user()->student->batch_id)
                ->where('chapter_id', '=', $chapter_id)
                ->where('start_at','<=', $currentDateTime)
                ->where('end_at','>=', $currentDateTime)
                ->where('links.type', $type)
                ->orderBy('start_at', 'desc')
                ->get(); 
                        // dd($Link);
        return $Link;
    }

    // public function getClassLink($subject_id=null){
      
    //     $currentDateTime = date('Y-m-d h:i:s');
    //     return $this->join('batch_link','batch_link.link_id','links.id')
    //                 ->join('students','students.batch_id','batch_link.batch_id')
    //                 ->where('students.user_id',Auth::user()->id)
    //                 ->where('end_at','>', $currentDateTime)
    //                 ->when(!empty($subject_id),function ($query) use ($subject_id){//if statement when()
    //                     return $query->where('subject_id',$subject_id);
    //                 })
    //                 ->where('type','Class')
    //                 ->orderBy('end_at', 'desc')
    //                 ->first();
    // }

    public function countAllTypeOfLink($type, $subject_id=null, $chapter_id=null){
        $currentDateTime = date('Y-m-d h:i:s');
        
        return $this->join('batch_link','batch_link.link_id','links.id')
                    ->where('batch_link.batch_id',auth()->user()->student->batch_id)
                    ->whereIn('type', $type)
                    ->where('start_at','<=', $currentDateTime)
                    ->where('end_at','>=', $currentDateTime)
                    ->when(!empty($subject_id),function ($query) use ($subject_id){//if statement when()
                        return $query->where('subject_id',$subject_id);
                    })
                    ->when(!empty($chapter_id),function ($query) use ($chapter_id){//if statement when()
                        return $query->where('chapter_id',$chapter_id);
                    })
                    ->count();
                    // ->get();
        
    }

    public function batch(){

        return $this->belongsToMany('App\Models\Batch')
                    ->withPivot('batch_id', 'link_id')
                    ->withTimestamps();
        // return $this->belongsToMany(Batch::class,'batch_link', 'batch_id','link_id');
    }

    public function subject(){

        return $this->belongsTo('App\Models\Subject','subject_id')  ;
    }

    public function chapter(){

        return $this->belongsTo('App\Models\Chapter','chapter_id')  ;
    }

    public function edited(){
        return $this->belongsTo('App\User','editedBy');
    }
}
