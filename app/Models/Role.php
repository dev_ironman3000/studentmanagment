<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public $roleId      = 2;
	protected $fillable = [
        'name',   
        'title',   
        'level',
    ];

    public function saveRole($Role){  
        
        $user = User::updateOrCreate(['id' => $Role['id']] ,$Role);
    }

    public function getAll(){  
        
        return $this->get();
    }
}
