<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Student;

class Notification extends Model
{
	public $roleId      = 2;
	protected $fillable = [
        'seen_by', 
        'message',
        'batch_id',
        'start_at',
        'end_at',
        'status',
    ];

    public function saveNotification($Notification){
        
        $reservationtime = explode('-', $Notification['reservationtime']);
        $Notification['status'] = $Notification['radiobutton'];
        $Notification['start_at'] = date("Y-m-d H:i:s", strtotime(trim($reservationtime[0])));
        $Notification['end_at']   = date("Y-m-d H:i:s", strtotime(trim($reservationtime[1])));
        //  dd($Notification);
        $Notification = Notification::updateOrCreate(['id' => $Notification['id']] ,$Notification);
    }

    public function getLatestNotification(){
        $user = auth()->user();
        $currentDateTime = date('Y-m-d h:i:s');
        $Notification = Notification::where('start_at','<=', $currentDateTime)
                                    ->where('end_at','>=', $currentDateTime)
                                    ->where('status',1)
                                    ->where('batch_id', $user->student->batch_id)
                                    ->orderBy('start_at', 'desc')
                                    ->limit(3)
                                    ->get();                          
        return $Notification;
    }

    public function viewAllNotification(){
        $user = auth()->user();
        $currentDateTime = date('Y-m-d h:i:s');
        $Notification = Notification::where('start_at','<=', $currentDateTime)
                                    ->where('end_at','>=', $currentDateTime)
                                    ->where('status',1)
                                    ->where('batch_id', $user->student->batch_id)
                                    ->orderBy('start_at', 'desc')
                                    ->get();                          
        return $Notification;
    }

    public function getAll(){  
        
        return $this->get();
    }
    public function notification()
    {
        return $this->hasOne('App\Models\Notification', 'id', 'id');
    }
    public function batch(){

        return $this->hasOne('App\Models\Batch', 'id', 'batch_id');
    }
}
