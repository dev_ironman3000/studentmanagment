<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\User;
use \DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Subject;


class Student extends Model
{
    public $roleId      = 3;
    public $demoRoleId  = 4;

	protected $fillable = [
        'user_id',
        'batch_id' 
    ];

    public function saveStudent($student){  

        
        DB::beginTransaction();
        try {
            // // if($request->hasFile('image')){
            // $image=$student['image'];
            // $name=$image->getClientOriginalName( );
            //     // dd($name);
            // // $request->image->store('upload/profilepic','public');
            //   $userImage= new User;
            //   $userImage->image=$name;
               // $userImage->save();
            $user  = array(
                    'first_name'   =>  $student['first_name'],
                    'last_name'    =>  $student['last_name'],
                    'email'        =>  $student['email'],
                    'role_id'      =>  $this->roleId,
                    'image'        =>  $student['image'],
                    'status'       =>  $student['status'],
                    'state'        =>  $student['state'],
                    'city'         =>  $student['city'],
                    'pincode'      =>  $student['pincode'],
                    'village'      =>  $student['village'],
                    'road'         =>  $student['road'],
                    'colony'       =>  $student['colony'],
                    'house_no'     =>  $student['house_no'],     
                    'landmark'     =>  $student['landmark'], 
                    'country'      =>  $student['country'],
                    'contact'      =>  $student['contact'],
                );
            
            if(!empty($student['password'])){
            	$user['password'] = Hash::make($student['password']);

            }
            // dd($user);
            $user = User::updateOrCreate(['id' => $student['user_id']] ,$user);
           
            $this->user_id  = $user->id;
            // $this->batch_id = $student['batch_id'];
            $this->save();
            $this->batch()->sync($student['batch_ids']);
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            //dd($e);
        }


    }

    public function createDemo($student){  
        
        DB::beginTransaction();
        try {
            
            $user  = array(
                    'first_name'   =>  $student['first_name'],
                    'last_name'    =>  $student['last_name'],
                    'email'        =>  $student['email'],
                    'role_id'      =>  $this->demoRoleId,
                    'status'       =>  $student['status'],
                );
          
            
            if(!empty($user['password'])){
                $user['password'] = Hash::make($user['password']);
            }
            
            $user = User::Create(['id' => $student['user_id']] ,$user);
            
            // $this->user()->attach($user->id);
            // $this->batch()->attach($student['batch_id']);
            $this->user_id  = $user->id;
            $this->batch_id = $student;
            $this->save();
            return $user;
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            dd($e);
        }

    }
    public function getAll(){  
        return $this->with('user','batch')->get();
    }

    public function getAllSubject(){
        return Subject::join('batch_subject', 'batch_subject.subject_id','subjects.id')
            ->join('batch_student', 'batch_subject.batch_id','batch_student.batch_id')
            ->where('batch_student.student_id', $this->id)
            ->select('subjects.*')
            ->groupBy('subjects.id')
            ->get();
   }
    
    public function getStudentData(){
        $user = auth()->user();
        $Student = user::where('role_id','>=',3);
        return $Student;
    }
//     public function getDemoStudentData(){
//        $user = auth()->user();
//        $DemoStudent = user::where('role_id',4);
//        return $DemoStudent;
//    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function batch(){

        return $this->belongsToMany('App\Models\Batch')
                    ->withPivot('batch_id', 'student_id')
                    ->withTimestamps();
        // return $this->hasOne('App\Models\Batch', 'id', 'batch_id');
    }

    

}
