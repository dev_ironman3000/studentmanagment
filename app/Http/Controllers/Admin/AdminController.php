<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Auth;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     
    
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewAll(){  
        
        return view('adminpannel.dashboard');
    }

    public function create(){  
        
        return view('adminpannel.dashboard');
    }

    public function update($id){  
        
        return view('adminpannel.dashboard');
    }
    

    public function delete($id){  
        
        return view('adminpannel.dashboard');
    }
    
    public function viewBatchwise($batch){  
        
        return view('adminpannel.dashboard');
    }

   
}


