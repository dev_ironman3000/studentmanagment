<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\Chapter;
use Exception;
use App\Exceptions\Handler as ExceptionHandler;
use Illuminate\Foundation\Exceptions\Handler;

class SubjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewAll(){

        $subjects =  new Subject;
        $subjects = $subjects->getAll();
        $chapters  = Chapter::all();
        
        return view('admin.subject.listall',compact('subjects','chapters'));
    }

    public function subjectWise($id){

        $subjects =  new Subject;
        $subjects = $subjects->all();
        
        return view('admin.subject.listall',compact('subjects'));
    }

    public function create(){  

          $subject  = new Subject;
        $chapters = new Chapter;
        $chapters = $chapters->getAll();
        return view('admin.subject.subject',compact('subject','chapters'));
    }

    public function update($id){  
    
        $subject = Subject::findOrFail($id);
        return view('admin.subject.subject',compact('subject'));
    }

    public function save(Request $request){ 
        
        $this->validate($request,[
            'name'              => 'required|string|max:255',
            'subject_category'  => 'required|string|max:255',
        ]);
    //dd($request->all());
        $subject= Subject::findOrNew($request->id);
        $subject->saveSubject($request->all());
        return redirect()->route('subjectviewAll');
    }

    public function delete($id){
           
        try{ 
        
            Subject::where('id',$id)->delete();
        
        }catch( \Exception $e) { 

            throw new Exception("Cannot delete Subject as it Included in Other Items, So Go back to Dashboard By Back button");

        }        
        return redirect()->route('subjectviewAll');
        
    }

}

?>
