<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chapter;
use App\Models\Subject;
use App\Rules\PriorityUnique;
use App\Http\Requests\ChapterStoreRequest;


class ChapterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewAll(){
        $chapters =  new Chapter;
        $chapters = $chapters->getAll();
        $subjects = Subject::all();
        
        return view('admin.chapter.listall',compact('chapters','subjects'));
    }

    public function subjectWise($subjectId){

        $chapters  = new Chapter;
        $chapters  = $chapters->getChapterBySubjectId($subjectId);
        $subjects  = Subject::all();
        // dd($chapters,$subjects);    
        return view('admin.chapter.listall',compact('chapters','subjects','subjectId'));
    }

    public function create(){  

        $chapter = new Chapter;
        $subjects = Subject::all();
    
        return view('admin.chapter.chapter',compact('chapter','subjects'));
    }

    public function update($id){ 

       $subjects = Subject::all();
       $chapter = Chapter::findOrFail($id);
       return view('admin.chapter.chapter',compact('chapter','subjects'));
       
    }

    public function save(ChapterStoreRequest $request){ 
        // dd($request->all());
        // $this->validate($request,[
        //     'chap_name'  => 'required|string|max:255',
        //     'chap_type'  => 'required|string|max:255',
        //     'chap_class' => 'required|array',
        //     'subject_id' =>  'required',
        //     // 'class'      =>  'required',
        //     'priority'   => 'required|numeric|min:1|max:100|unique:chapters,priority,'.$request->id.',id,subject_id,'.$request->subject_id.',chap_class,'.$request->chap_class,

        //     ]);
        
        $chapter= new Chapter;
        $chapter->saveChapter($request->all());
        return redirect('admin/chapter/all');
    }

  
    public function delete($id){

        $chapter = Chapter::find($id)->delete();
        return redirect('admin/chapter/all');
    }

}


