@extends('layouts.student')

@section('title', 'Dashboard')

@section('content_header')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('student/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Subjects</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.container-fluid -->
@stop
@section('content')


<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

        @foreach($chapters  as  $chapter)
            @if($chapter->links->count() > 0)
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h5>{{$chapter->chap_name}}</h5>

                    <p>{{$chapter->subject->name}}</p>
                  </div>
                  
                  <div class="icon">
                    <i class="ion ion-book"> {{$chapter->links->count()}}</i>
                  </div>
                  <a href="{{url('student/showTodayLink/'.$chapter->id)}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
          <!-- ./col -->
          @endif
          @endforeach
          <!-- ./col -->
        </div>
      </div>
</section>
@stop

@section('css')

@stop

@section('js')


@stop