<form role="form" action="{{url('/admin/student/save')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <input type="hidden" name="user_id" value="{{ $student->user_id }}">
  <input type="hidden" name="id" value="{{ $student->id }}">
  <div class="card-body">
    <div class="form-group @if($errors->has('first_name')) has-error @endif">
      @php
      $value = old('first_name',$student->user->first_name);
      @endphp
      <div class="form-group">
        <label for="inputFname">Firstname</label>
        <input type="text" class="form-control" name="first_name" id="inputFname" value='{{$value}}'
          placeholder="Enter Firstname">
      </div>
      @if ($errors->has('first_name'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('first_name') }}</strong>
      </p>
      @endif
    </div>
    <div class="form-group @if($errors->has('last_name')) has-error @endif">
      @php
      $value = old('last_name',$student->user->last_name);
      @endphp
      <div class="form-group">
        <label for="inputLname">Lastname</label>
        <input type="text" class="form-control" name="last_name" id="inputLname" value='{{$value}}'
          placeholder="Enter Lastname">
      </div>
      @if ($errors->has('last_name'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('last_name') }}</strong>
      </p>
      @endif
    </div>



    <div class="form-group @if($errors->has('email')) has-error @endif">
      @php
      $value = old('email',$student->user->email);
      @endphp
      <div class="form-group">
        <label for="inputEmail1">Email</label>
        <input type="email" class="form-control" name="email" id="inputEmail1" value='{{$value}}'
          placeholder="Enter email">
      </div>
      @if ($errors->has('email'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('email') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('batch_id')) has-error @endif">
      @php
      $value = old('batch_ids',$student->batch->pluck('id')->toArray()); 
      @endphp
      <div class="form-group">
        <label for="inputclass1">Batch</label>
        <select class="custom-select select2" name="batch_ids[]" multiple>
          @foreach($batches as $batch)
          <option @if(in_array($batch->id,$value)) selected="selected" @endif value="{{$batch->id}}">
            {{$batch->batch_name}}
          </option>
          @endforeach
        </select>
      </div>
      @if ($errors->has('batch_id'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('batch_id') }}</strong>
      </p>
      @endif
     </div>
      

     <div class="form-group @if($errors->has('country')) has-error @endif">
      @php
      $value = old('country',$student->user->country);
      @endphp
      <div class="form-group">
        <label for="inputCountry">Country</label>
        <select class="custom-select select2" name="country">
         
          <option selected="selected" value="India">
           India
          </option>
         
        </select>
      </div>
      @if ($errors->has('country'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('country') }}</strong>
      </p>
      @endif
    </div>
  

    <div class="form-group @if($errors->has('state')) has-error @endif">
      @php
      $value = old('state',$student->user->state);
      @endphp
      <div class="form-group">
        <label for="inputState">State</label>
        <input type="text" class="form-control" name="state" id="state" value='{{$value}}'
          placeholder="State">
         
      </div>
      @if ($errors->has('state'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('state') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('city')) has-error @endif">
      @php
      $value = old('city',$student->user->city);
      @endphp
      <div class="form-group">
        <label for="inputCity">City</label>
        <input type="text" class="form-control" name="city" id="city" value='{{$value}}'
          placeholder="City">
         
      </div>
      @if ($errors->has('city'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('city') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('pincode')) has-error @endif">
      @php
      $value = old('pincode',$student->user->pincode);
      @endphp
      <div class="form-group">
        <label for="inputPincode">Pincode</label>
        <input type="text" class="form-control" name="pincode" id="pincode" value='{{$value}}'
          placeholder="Pincode">
         
      </div>
      @if ($errors->has('pincode'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('pincode') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('village')) has-error @endif">
      @php
      $value = old('village',$student->user->village);
      @endphp
      <div class="form-group">
        <label for="inputVillage">Village</label>
        <input type="text" class="form-control" name="village" id="village" value='{{$value}}'
          placeholder="village">
         
      </div>
      @if ($errors->has('village'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('village') }}</strong>
      </p>
      @endif
    </div>




    <div class="form-group @if($errors->has('road')) has-error @endif">
      @php
      $value = old('road',$student->user->road);
      @endphp
      <div class="form-group">
        <label for="inputRoad">Road</label>
        <input type="text" class="form-control" name="road" id="road" value='{{$value}}'
          placeholder="Road">
         
      </div>
      @if ($errors->has('road'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('road') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('colony')) has-error @endif">
      @php
      $value = old('colony',$student->user->colony);
      @endphp
      <div class="form-group">
        <label for="inputColony">Colony</label>
        <input type="text" class="form-control" name="colony" id="colony" value='{{$value}}'
          placeholder="Colony">
         
      </div>
      @if ($errors->has('colony'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('colony') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('house_no')) has-error @endif">
      @php
      $value = old('house_no',$student->user->house_no);
      @endphp
      <div class="form-group">
        <label for="inputHouseNo">House No</label>
        <input type="text" class="form-control" name="house_no" id="house_no" value='{{$value}}'
          placeholder="House No.">
         
      </div>
      @if ($errors->has('house_no'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('house_no') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('landmark')) has-error @endif">
      @php
      $value = old('landmark',$student->user->landmark);
      @endphp
      <div class="form-group">
        <label for="inputLandmark">Landmark</label>
        <input type="text" class="form-control" name="landmark" id="landmark" value='{{$value}}'
          placeholder="Landmark">
         
      </div>
      @if ($errors->has('landmark'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('landmark') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('contact')) has-error @endif">
      @php $value = old('contact',$student->user->contact); @endphp
      <div class="form-group">
        <label for="contact">Contact</label>
        <input type="text" class="form-control" name="contact" id="contact" value='{{$value}}'
          placeholder="contact">
         
      </div>
      @if ($errors->has('contact'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('contact') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group  @if($errors->has('password')) has-error @endif">
      <div class="form-group">
        <label for="inputPassword">Password</label>
        <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Enter Password" value="">
      </div>
      @if ($errors->has('password'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('password') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group  @if($errors->has('password_confirmation')) has-error @endif">
      <div class="form-group">
        <label for="inputconpassword">Confirm Password</label>
        <input type="password" class="form-control" name="password_confirmation" id="inputconpassword"
          placeholder="Confirm Password">
      </div>
      @if ($errors->has('password_confirmation'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('password_confirmation') }}</strong>
      </p>
      @endif
    </div>
  </div>
                <div class="form-group @if($errors->has('radioSuccess1')) has-error @endif">
                      @php 
                       $value = old('status',$student->user->status); 
                      @endphp
                      
                      <div class="form-group">
                        <label for="inputFname">Status</label>

                        <div class="col-sm-7 ">
                    <!-- radio -->
                    <div class="form-group clearfix">
                      <div class="icheck-success d-inline">
                          <label for="radioSuccess1">
                            Active
                          </label>
                      </div>
                      <div class="icheck-success d-inline">
                      <input type="radio" name="radiobutton" id="radioSuccess1" value="1"  @if($student->user->status == 1) checked=""  @endif>
                       
                        <label for="radioSuccess1">
                        </label>
                      </div>

                        <div class="icheck-success d-inline">
                          <label for="radioSuccess2">
                            Inactive
                          </label>
                        </div>

                      <div class="icheck-danger d-inline">
                      <input type="radio" name="radiobutton" id="radioSuccess2" value="0" @if($student->user->status == 0) checked=""  @endif>
                       <label for="radioSuccess2">
                        </label>
                      </div>
                      
                    </div>
                  </div></div></div>
    <div class="form-group  @if($errors->has('image')) has-error @endif">
      <div class="input-group">
        <div class="custom-file col-md-7">
          <input type="hidden" name="profile_pic" value="{{$student->user->image}}">
          <input type="file" name="image" class="custom-file-input ">
          <lable id="pic" class="custom-file-label">Choose Profile Picture</lable>
        </div>
      </div>
      @if ($errors->has('image'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('image') }}</strong>
      </p>
      @endif
    </div>
  </div>

  <!-- /.card-body -->
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
@section('plugins.radiobutton', true)

