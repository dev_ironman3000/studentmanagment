
@if($chapters->count() > 0)

	<option value="">--Select Chapter--</option>
	@foreach($chapters as $chapter)
		<option value="{{$chapter->id}}">{{$chapter->chap_name}}</option>
	@endforeach

@else

	<option>No chapter found</option>

@endif

                       